from collections import Counter
import math
from matplotlib import pyplot as plt
from ch4 import sum_of_squares

num_friends = [100, 49, 41, 40, 25, 26, 13, 23, 26, 19, 25, 25, 1, 0, 23, 15
	# ... and so forth
	]

# a simple bar chart of friend counts
	
friend_counts = Counter(num_friends)
xs = range(101)
ys = [friend_counts[x] for x in xs]
plt.bar(xs,ys)
plt.axis([0,101,0,25])
plt.title("Histogram of Friend Counts")
plt.xlabel("# of friends")
plt.ylabel("# of people")
plt.show()

# let's break it down statistically
num_points = len(num_friends)
print(num_points)
largest_value = max(num_friends)
print(largest_value)
smallest_value = min(num_friends)
print(smallest_value)

# finding the central tendency
def mean(x):
	return sum(x) / len(x)
	
print(mean(num_friends))

def median(v):
	"""finds the middle most value of v"""
	n = len(v)
	sorted_v = sorted(v)
	midpoint = n // 2
	if n % 2 == 1:
		return sorted_v[midpoint]
	else:
		lo =  midpoint - 1
		hi = midpoint
		return (sorted_v[lo] + sorted_v[hi])/2
		
print(median(num_friends))

def quantile(x, p):
	"""returns the pth-percentile value in x"""
	p_index = int(p * len(x))
	return sorted(x)[p_index]
	
print(quantile(num_friends, 0.10))
print(quantile(num_friends, 0.25))
print(quantile(num_friends, 0.75))
print(quantile(num_friends, 0.90))

def mode(x):
	""" returns a list, might be more than one mode"""
	counts = Counter(x)
	max_count = max(counts.values())
	return sorted([x_i for x_i, count in counts.items()
		if count == max_count])
		
print(mode(num_friends))

# Dispersion

def data_range(x):
	return max(x) - min(x)
	
print(data_range(num_friends))

def de_mean(x):
	"""translate x by subtracting its mean so they result has mean """
	x_bar = mean(x)
	return [x_i - x_bar for x_i in x]
	
def variance(x):
	n = len(x)
	deviations = de_mean(x)
	return sum_of_squares(deviations) / (n - 1)
	
print(variance(num_friends))

def standard_deviation(x):
	return math.sqrt(variance(x))
	
print(standard_deviation(num_friends))

