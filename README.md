# Description
This repository presents notes and project code for Data Science From Scratch by Grus in Python 3. Author's repository can be found [here](https://github.com/joelgrus/data-science-from-scratch/tree/master/code-python3).
