# Task 1 - finding key connectors
from __future__ import division # integer division is lame; used for average friends
from collections import Counter # not loaded by default; used for friend of friend exercise
from collections import defaultdict # used to build dict for interest matching
from matplotlib import pyplot as plt # for plotting

#Users is a dict with each user's ID and name
users = [
	{ "id": 0, "name": "Hero" },
	{ "id": 1, "name": "Dunn" },
	{ "id": 2, "name": "Sue" },
	{ "id": 3, "name": "Chi" },
	{ "id": 4, "name": "Thor" },
	{ "id": 5, "name": "Clive" },
	{ "id": 6, "name": "Hicks" },
	{ "id": 7, "name": "Devin" },
	{ "id": 8, "name": "Kate" },
	{ "id": 9, "name": "Klein" }
]

#friendships is a list representing friendhip ID pairs
friendships = 	[(0,1), (0,2), (1,2), (1,3), (2,3), (3,4),
				(4,5), (5,6), (5,7), (6,8), (7,8), (8,9)]
				
# making a list of friends for each user
for user in users:
	user["friends"] = []

# populate the lists with friendships data
for i, j in friendships:
	# this works because users[i] is the user whose id is i
	users[i]["friends"].append(users[j]) # add i as a friend of j
	users[j]["friends"].append(users[i]) # add j as a friend of i

# collecting total number of connections by summing length of each friends list
def number_of_friends(user):
	"""how many friends does _user_ have?"""
	return len(user["friends"])		# length f friend_ids list
	
total_connections = sum(number_of_friends(user)
						for user in users)

print("Total connections is", total_connections) # should be 24

num_users = len(users)
avg_connections = total_connections / num_users
print("Average number of connections is", avg_connections) # should be 2.4
print('\n')
# sorting users from most friends to least friends
# create a list (user_id, number_of_friends)
num_friends_by_id = [(user["id"], number_of_friends(user))
					for user in users]
								
results = sorted(num_friends_by_id,								# gets it sorted
	key=lambda pair: pair[1],							# by num_friends
	reverse=True)										# largest to smallest 

# this can be considered the network metric degree centrality
for result in results:
	print("User ", result[0]," has ",result[1]," friends.")

print("\n")
# calculating friends of friends
def friends_of_friends_ids_bad(user):
	# "foaf" is short for "friend of a friend"
	return [foaf["id"]
			for friend in user["friends"]	#for each of user's friends
			for foaf in freiend["friends"]]	# get each of _their_ friends

print("User 0 is friends are friends with ",[friend["id"] for friend in users[0]["friends"]])
print("User 1 is friends are friends with ",[friend["id"] for friend in users[1]["friends"]])
print("User 2 is friends are friends with ",[friend["id"] for friend in users[2]["friends"]])
print("\n")
# producing count of mutual friends
def not_the_same(user, other_user):
	"""two users are not the same if they have different ids"""
	return user["id"] != other_user["id"]
	
def not_friends(user, other_user):
	"""other_user is not a riend if he's not in user["friends"];
	that is, if he's not_the_same as all the people in user["friends"]"""
	return all(not_the_same(friend, other_user)
		for friend in user["friends"])
def friend_of_friend_ids(user):
	return Counter(foaf["id"]
		for friend in user["friends"]	# for each of my friends	
		for foaf in friend["friends"]	# count *their* friends
		if not_the_same(user, foaf)		# who aren't me
		and not_friends(user, foaf))	# and aren't my friends
		
print(friend_of_friend_ids(users[3]))
print("\n")

# finding users with similar interests
interests = [
	(0, "Hadoop"), (0, "Big Data"), (0, "HBase"), (0, "Java"), (0, "Spark"), (0, "Storm"), (0, "Cassandra"),
	(1, "NoSQL"), (1, "MongoDB"), (1, "Cassandra"), (1, "HBase"), (1, "Postgres"),
	(2, "Python"), (2, "scikit-learn"), (2, "scipy"), (2, "numpy"), (2, "statsmodels"), (2, "pandas"), 
	(3, "R"), (3, "regression"), (3, "probability"), (3, "statistics"), (3, "Python"), 
	(4, "machine learning"), (4, "regression"), (4, "decision trees"), (4, "libsvm"),
	(5, "Python"), (5, "R"), (5, "Java"), (5, "C++"), (5, "Haskell"), (5, "programming lanuages"), 
	(6, "statistics"), (6, "probability"), (6, "mathematics"), (6, "theory"),
	(7, "machine learning"), (7, "scikit-learn"), (7, "Mahout"), (7, "neural networks"),
	(8, "neural networks"), (8, "deep learning"), (8, "Big Data"), (8, "artificial intelligence"),
	(9, "Hadoop"), (9, "Java"), (9, "MapReduce"), (9, "Big Data")
]

def data_scientists_who_like(target_interest):
	return [user_id
		for user_id, user_interest in interests
		if user_interest == target_interest]
		
# the above function works, but has to examine the whole list every time, which could be time and resource intensive
# therefore, building an index of interests to users

# keys are interests, values are lists of user_ids with that interest
user_ids_by_interest = defaultdict(list)

for user_id, interest in interests:
    user_ids_by_interest[interest].append(user_id)

# keys are user_ids, values are lists of interests for that user_id

interests_by_user_id = defaultdict(list)

for user_id, interest in interests:
    interests_by_user_id[user_id].append(interest)

def most_common_interests_with(user_id):
    return Counter(interested_user_id
        for interest in interests_by_user["user_id"]
        for interested_user_id in users_by_interest[interest]
        if interested_user_id != user_id)

# Figuring out data scientist earnings
		
salaries_and_tenures = [(83000, 8.7), (88000, 8.1),
                        (48000, 0.7), (76000, 6),
                        (69000, 6.5), (76000, 7.5),
                        (60000, 2.5), (83000, 10),
                        (48000, 1.9), (63000, 4.2)]

def make_chart_salaries_by_tenure():
    tenures = [tenure for salary, tenure in salaries_and_tenures]
    salaries = [salary for salary, tenure in salaries_and_tenures]
    plt.scatter(tenures, salaries)
    plt.xlabel("Years Experience")
    plt.ylabel("Salary")
    plt.show()

make_chart_salaries_by_tenure()
	
# keys are years, values are lists of the salaries for each tenure
salary_by_tenure = defaultdict(list)

for salary, tenure in salaries_and_tenures:
	salary_by_tenure[tenure].append(salary)

# keys are years, each value is average salary for that tenure
average_salary_by_tenure = {
	tenure: sum(salaries) / len(salaries)
	for tenure, salaries in salary_by_tenure.items()
}

print("Average Salary by Tenure")
print(average_salary_by_tenure)

# bucketing tenures
def tenure_bucket(tenure):
	if tenure < 2:
		return "less than two"
	elif tenure < 5:
		return "between 2 and 5"
	else:
		return "more than five"
		
# keys are tenure buckets, values are lists of salaries for that bucket
salary_by_tenure_bucket = defaultdict(list)

for salary, tenure in salaries_and_tenures:
	bucket = tenure_bucket(tenure)
	salary_by_tenure_bucket[bucket].append(salary)
	
# keys are tenure buckets, values are avverage salary for that bucket
average_salary_by_bucket = {
	tenure_bucket: sum(salaries) / len(salaries)for tenure_bucket, salaries in salary_by_tenure_bucket.items()
}

print("Average Salary By Bucket:")
print(average_salary_by_bucket)

# crude model for predicting paid account from years of experience
def predict_paid_or_unpaid(years_experience):
	if years_experience < 3.0:
		return "paid"
	elif years_experience < 8.5:
		return "unpaid"
	else:
		return "paid"
		
print(predict_paid_or_unpaid(2))
print(predict_paid_or_unpaid(23))
print(predict_paid_or_unpaid(5))

# Topics of interest
# counting popularity of interests

words_and_counts = Counter(word									# counts words
							for user, interest in interests		  
							for word in interest.lower().split()) # make words lowercase and split them out
	
for word, count in words_and_counts.most_common():
	if count > 1:
		print(word, count)