import math
from functools import *

### Vectors

height_weight_age = [70, 	#inches
					 170, 	#pounds
					 40 ]	#years

grades = [	95,	#exam1
			89,	#exam2
			75,	#exam3
			62]	#exam4
			
# vector addition
def vector_add(v,w):
	"""adds corresponding elements"""
	return [v_i + w_i
			for v_i, w_i in zip(v,w)]

#print(vector_add(grades, grades))			
			
def vector_subtract(v,w):
	"""subtracts corresponding elements"""
	return [v_i - w_i
			for v_i, w_i in zip(v,w)]

#print(vector_subtract(grades, grades))			

# sum vectors componentwise

## Version 1
#def vector_sum(vectors):
#	"""sums all corresponding elements"""
#	result = vectors[0]
#	for vector in vectors[1:]:
#		result = vector_add(result, vector)
#	return result

## Version 2
#def vector_sum(vectors):
#	return reduce(vector_add, vectors)

## Version 3
vector_sum = partial(reduce, vector_add)

def scalar_multiply(c, v):
	"""c is a number, v is a vector"""
	return [c*v_i for v_i in v]

def vector_mean(vectors):
	"""compute the vector whose ith element is the mean of the ith element of the input vectors"""
	n = len(vectors)
	return scalar_multiply(1/n, vector_sum(vectors))
	
# dot product - the dot product of two vectors is the sum of their componentwise products
def dot(v, w):
	"""v_i * w_i + .... v_n * w_n"""
	return sum(v_i * w_i
				for v_i, w_i in zip(v, w))

def sum_of_squares(v):
	"""v_i * v_i + ... + v_n * v_n"""
	return dot(v,v)
	
def magnitude(v):
	return math.sqrt(sum_of_squares(v))
	
def squared_distance(v,w):
	"""(v_1 - w_1)^2 + ... + (v_n - w_n)^2"""
	return sum_of_squares(vector_subtract(v,w))
	
#def distance(v,w):
#	return math.sqrt(squared_distance(v,w))
	
def distance(v,w):
	return magnitude(vector_subtract(v,w))
	
# Matrices, AKA 2 dimensional vectors
# represented as lists of lists

A = [[1,2,3],
	[4,5,6]]
	
B = [[1,2],
	[3,4],
	[5,6]]
	
def shape(A):
	num_rows = len(A)
	num_cols = len(A[0]) if A else 0
	return num_rows, num_cols
	
def get_row(A, i):
	return A[i]
	
def get_column(A, j):
	return [A_i[j]
		for A_i in A]
		
def make_matrix(num_rows, num_cols, entry_fn):
	"""returns a matrix of num_rows x num_cols
	whose (i,j)th entry is entry_fn(i,j)"""
	return[[entry_fn(i,j)			# give i, create a list
		for j in range(num_cols)]	#  [entry_fn(i, 0, ... ]
		for i in range(num_rows)]	#	create on list for each i

def is_diagonal(i,j):
	"""1's on the diagonal, 0's everywhere else"""
	return 1 if i == j else 0

# make a 5x5 identity matrix
identity_matrix = make_matrix(5,5, is_diagonal)

print(identity_matrix)